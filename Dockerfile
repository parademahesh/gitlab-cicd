# You can change this base image to anything else
# But make sure to use the correct version of Java
FROM adoptopenjdk/openjdk11:alpine-jre

# Simply the artifact path
ARG artifact=target/final-pro.jar
WORKDIR /opt/final-pro
COPY ${artifact} final-pro.jar
# This should not be changed
ENTRYPOINT ["java","-jar","final-pro.jar"]
